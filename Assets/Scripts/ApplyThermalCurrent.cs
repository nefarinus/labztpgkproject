﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyThermalCurrent : MonoBehaviour
{
    [SerializeField] private float verticalForceAmount;

    private void OnTriggerStay(Collider other) {
        other.gameObject.transform.parent.transform.position += new Vector3(0, verticalForceAmount * Time.deltaTime, 0);
    }
}
