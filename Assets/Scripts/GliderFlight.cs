﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GliderFlight : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float liftCoefficient;
    [SerializeField] private float dragCoefficient;
    [SerializeField] private float gravCoefficient;
    [SerializeField] private float manuverabilityRequiredSpeed;

    void Start()
    {
        rb.AddForce(transform.forward * 1000);
    }

    private void Update()
    {
        applyLift();
        applyDrag();
        applyGravity();
        applyVelocityDirectionCorrection();
    }

    private void applyVelocityDirectionCorrection()
    {
        if (rb.velocity.magnitude > manuverabilityRequiredSpeed)
        {
            rb.velocity = transform.forward * rb.velocity.magnitude;
        }
    }

    private void applyLift()
    {
        float liftValue = rb.velocity.z * liftCoefficient;
        rb.velocity += transform.up * liftValue * Time.deltaTime;
    }

    private void applyDrag()
    {
        float dragValue = rb.velocity.magnitude * dragCoefficient;
        rb.velocity += -transform.forward * dragValue * Time.deltaTime;
    }

    private void applyGravity()
    {
        float gravityValue = rb.mass * gravCoefficient;
        rb.velocity += -Vector3.up * gravityValue * Time.deltaTime;
        transform.position -= new Vector3(0, gravCoefficient * 0.5f * Time.deltaTime, 0);
    }

}
