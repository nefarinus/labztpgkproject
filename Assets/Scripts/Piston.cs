﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piston : MonoBehaviour
{
    [SerializeField] private float forceAmount;
    [SerializeField] private float delay;
    [SerializeField] private Rigidbody rb;

    private bool pistonEnabled = true;

    private Coroutine pistonMovement;

    private void Start()
    {
        pistonMovement = StartCoroutine("pistonAddforce");
    }

    private IEnumerator pistonAddforce()
    {
        while (pistonEnabled)
        {
            rb.AddForce(-transform.up * forceAmount);
            yield return new WaitForSeconds(delay);
        }
    }
}
