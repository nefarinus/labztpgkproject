﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckpointCrash : MonoBehaviour
{

    public UnityEvent onCrash;

    private void OnCollisionEnter(Collision other) {
        onCrash.Invoke();        
    }
}
