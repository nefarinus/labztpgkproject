﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steering : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float verticalRotationModiffier;
    [SerializeField] private float horizontalRotationModiffier;
    [SerializeField] private float stabilizerRotationModiffier;

    [SerializeField] private float verticalThreshold;
    [SerializeField] private float horizontalThreshold;

    private void FixedUpdate()
    {
        if (Mathf.Abs(Input.GetAxis("Vertical")) > verticalThreshold)
        {
            Quaternion addRotation = Quaternion.identity;
            addRotation.eulerAngles = new Vector3(Input.GetAxis("Vertical") * verticalRotationModiffier, 0, 0);
            rb.rotation *= addRotation;
        }

        if (Mathf.Abs(Input.GetAxis("Horizontal")) > horizontalThreshold)
        {
            Quaternion addRotation = Quaternion.identity;
            addRotation.eulerAngles = new Vector3(0, Input.GetAxis("Horizontal") * stabilizerRotationModiffier, -Input.GetAxis("Horizontal") * horizontalRotationModiffier);
            rb.rotation *= addRotation;
        } 
        else {
       
            rb.rotation = Quaternion.Slerp(rb.rotation, Quaternion.Euler(rb.rotation.eulerAngles.x, rb.rotation.eulerAngles.y, 0), Time.deltaTime * 0.5f);
        
        }

        


    }
}
