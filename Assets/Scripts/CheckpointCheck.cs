﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckpointCheck : MonoBehaviour
{
    public bool completed;
    public UnityEvent onCheckpointCompleted;

    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Material completedMat;
    [SerializeField] private Material incompletedMat;


    private void OnTriggerEnter(Collider other)
    {

        if (completed == false)
        {
            completed = true;
            meshRenderer.material = completedMat;
            onCheckpointCompleted.Invoke();
        }
    }
}
