﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour
{
    [SerializeField] private List<CheckpointCheck> checkpoints;
    [SerializeField] private UIController UIctrl;

    public void OnCheckpointCompleted() {
        bool allCompleted = true;
        int incomplete = 0;

        foreach (CheckpointCheck c in checkpoints) {
            allCompleted &= c.completed;
            incomplete += c.completed ? 0 : 1;
        }

        UIctrl.UpdateCheckpointCount(incomplete);

        if (allCompleted == true) {
            Time.timeScale = 0;
            UIctrl.OnWin();
        }
    }

    public void OnCrash() {
        Time.timeScale = 0;
        UIctrl.OnLose();
    }
}
