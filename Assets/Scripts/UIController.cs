﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] private TMP_Text altitudeText;
    [SerializeField] private TMP_Text velocityText;
    [SerializeField] private TMP_Text checkPointsRemaining;

    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject loseScreen;

    [SerializeField] private Rigidbody glider_rb;

    private void Update()
    {
        velocityText.text = "Velocity: " + glider_rb.velocity.magnitude;
        altitudeText.text = "Altitude: " + glider_rb.transform.position.y;
    }

    public void OnWin()
    {
        winScreen.SetActive(true);
    }

    public void OnLose()
    {
        loseScreen.SetActive(true);
    }

    public void UpdateCheckpointCount(int remaining)
    {
        checkPointsRemaining.text = "Checkpoints remaining: " + remaining;
    }
}
